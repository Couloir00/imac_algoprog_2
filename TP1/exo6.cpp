#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant = nullptr;
};

struct Liste{
    Noeud* premier = nullptr;
    // your code
};

struct DynaTableau{
    int* donnees;
    int nbCasesR;
    int capacite;
    // your code
};


void initialise(Liste* liste)
{
    liste->premier=nullptr;
}

bool est_vide(const Liste* liste)
{
     if (liste->premier == nullptr){
         return true;
     }
     return false;
}

void ajoute(Liste* liste, int valeur)
{
    if (liste->premier == nullptr){
        liste->premier = new Noeud;
        liste->premier->donnee = valeur;
    }
    else {
        Noeud* Prochain = liste->premier;
        while(Prochain->suivant != nullptr){
            Prochain=Prochain->suivant;
        }
        Noeud* NewNode = new Noeud;
        NewNode->donnee = valeur;
        Prochain->suivant = NewNode;
    }    
}

void affiche(const Liste* liste)
{
    if(est_vide(liste)){
        std::cout << "La liste est vide ahah !" << std::endl;
        return;
    }
    Noeud* next = liste->premier;
    while(next->suivant != nullptr){
        std::cout << next->donnee << std::endl;
        next = next->suivant;
    }
    std::cout<< next->donnee << std::endl;
}

int recupere(const Liste* liste, int n)
{   
    int i=0;
    Noeud* Valeur = liste->premier;
    while (i != n && Valeur->suivant !=nullptr){
        Valeur=Valeur->suivant;
        i++;
    }
    if (i!=n){
        return -1;
    }
    return Valeur->donnee;
   
}

int cherche(const Liste* liste, int valeur)
{
    int count=0;
    Noeud* Cherche = liste->premier;

    if (Cherche->donnee == valeur){
        return count;
    }

    while(Cherche->suivant->donnee != valeur && Cherche->suivant != nullptr){
        count ++;
        Cherche=Cherche->suivant;
    }

    if (Cherche->suivant->donnee == valeur){
        return count+1;
    } 
    else{
        return -1;
    }
    
}

void stocke(Liste* liste, int n, int valeur)
{
    int i=0;
    Noeud* trouve = liste->premier;
    while(i != n && trouve->suivant !=nullptr){
        trouve=trouve->suivant;
        i++;
    }
    if (i!=n){
        return;
    }
    trouve->donnee=valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    tableau->donnees[tableau->nbCasesR]=valeur;
    tableau->nbCasesR ++;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;
    //j'aimerai faire avec new
    tableau->donnees= (int*) malloc (sizeof(int)*tableau->capacite);
    tableau->nbCasesR=0;
}

bool est_vide(const DynaTableau* liste)
{
    if (liste->nbCasesR==0){
        return true;
    }
    else{
        return false;
    }
}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i<tableau->nbCasesR; i++){
        std::cout << i << "." << tableau->donnees[i] << std::endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    return tableau->donnees[n];
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int i=0;
    while (tableau->donnees[i]!=valeur && i<tableau->nbCasesR){
        i++;
    }
    if(tableau->donnees[i]==valeur){
        return i;
    }
    else{
        return -1;
    }
    
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n]=valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    //à la fin 
    ajoute(liste,valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{   
    int firstValue;
    Noeud* first= liste->premier;
    firstValue=first->donnee;
    liste->premier=liste->premier->suivant;
    return firstValue;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    //on ajoute au dessus
    Noeud* auTop=new Noeud();
    auTop->donnee=valeur;
    liste->premier=auTop;
}


//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    //dernière valeur au dessus
    int lastValue = liste->premier->donnee;
    liste->premier = liste->premier->suivant;
    return lastValue;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans le tableau à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
