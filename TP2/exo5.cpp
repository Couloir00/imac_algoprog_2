#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	if (origin.size()>1){
		// initialisation
		Array& first = w->newArray(origin.size()/2);
		Array& second = w->newArray(origin.size()-first.size());
		
		// split
		for (int i=0; i < (origin.size()/2); i++){
			first[i]=origin[i];
		}
		for (int i=(origin.size()/2); i < origin.size(); i++){
			second[i-(origin.size()/2)]=origin[i];
		}

		// recursiv splitAndMerge of lowerArray and greaterArray
		splitAndMerge(first);
		splitAndMerge(second);
		// merge
		merge(first,second,origin);
	}
}

void merge(Array& first, Array& second, Array& result)
{
	int i=0, j=0;
	for (int count=0;count<(first.size() + second.size());count++){
		if(i<first.size() && j<second.size()){
			if (first[i] < second[j]){
				result[count]=first[i];
				i++;
			}
			else{
				result[count]=second[j];
				j++;
			}
		}
		else if (i<first.size()){
			result[count]=first[i];
			i++;
		}
		else if (j<second.size()){
			result[count]=second[j];
			j++;
		}
		
	}
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
