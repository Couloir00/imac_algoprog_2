#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int _value)
    {
        // init initial node without children
        this->left = nullptr;
        this->right =nullptr;
        this->value=_value;        
    }

	void insertNumber(int _value) {
        // create a new node and insert it in right or left child
        if(this->value > _value){
            if(this->left == nullptr){
                this->left = new SearchTreeNode(_value);
            }
            else{
                this->left->insertNumber(_value);
            }
        }
        else{
            if(this->right == nullptr){
            this->right =new SearchTreeNode(_value);
            }
            else{
                this->right->insertNumber(_value);
            }
        }   
    }
	uint height() const	{
        uint leftHeight = 0,rightHeight = 0;
        //If there is no child, return just 1
        if (this->left == nullptr && this->right==nullptr){
            return 1;
        }
        if( this->left != nullptr){
            leftHeight = this->left->height()+1;
        }
        if( this->right != nullptr){
            rightHeight = this->right->height()+1;
        }
        // should return the maximum height between left child and
        // right child +1 for itself. 
        if(leftHeight < rightHeight){
            return rightHeight;
        }
        return leftHeight;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        uint count=0;
         if (this->left == nullptr && this->right==nullptr){
            return 1;
        }
         if( this->left != nullptr){
             count=count+this->left->nodesCount();
        }
        if( this->right != nullptr){
            count=count+this->right->nodesCount();
        }
        return count+1;
	}

	bool isLeaf() const {
        if (this->left == nullptr && this->right==nullptr){
            return true;
        }
        // return True if the node is a leaf (it has no children)
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(isLeaf()){
            leaves[leavesCount]=this; //this->value ?
            leavesCount++;
        }
        else{
            if(left!=nullptr){
                this->left->allLeaves(leaves,leavesCount);
            }
            if(right!=nullptr){
            this->right->allLeaves(leaves,leavesCount);
            }
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        
       //fils gauche 
        if (this->left!=nullptr){
            this->left->inorderTravel(nodes,nodesCount);
        }
        //parent
        nodes[nodesCount]=this;
        nodesCount ++;
        //fils droit
        if (this->right!=nullptr){
            this->right->inorderTravel(nodes,nodesCount);
        }
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        //parent
        nodes[nodesCount]=this;
        nodesCount ++;
        //fils gauche
        if (this->left!=nullptr){
            this->left->preorderTravel(nodes,nodesCount);
        }
        //fils droit
        if (this->right!=nullptr){
            this->right->preorderTravel(nodes,nodesCount);
        }
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        //fils gauche
        if (this->left!=nullptr){
            this->left->postorderTravel(nodes,nodesCount);
        }
        //fils droit
        if (this->right!=nullptr){
            this->right->postorderTravel(nodes,nodesCount);
        }
        //parent
        nodes[nodesCount]=this;
        nodesCount ++;
	}

	Node* find(int _value) {
        // find the node containing value
        if(this->value==_value){
            return this;
        }
        else if(this->value>_value && this->left!=nullptr){
            return this->left->find(_value);
        }
        else if(this->value<_value && this->right!=nullptr){
            return this->right->find(_value);
        }
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
