#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
    for(int j=0; j<toSort.size();j++){
        int min=j;
        for(int i=j; i<toSort.size();i++){
            if (toSort[i]<=toSort[min]){
                min=i;
            }
        }
        toSort.swap(j,min);
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
