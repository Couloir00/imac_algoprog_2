#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	for (int i=1; i<toSort.size(); i++){
		for (int j=1; j<toSort.size()-i; j++)
		if (toSort[j]<toSort[j-1]){
			toSort.swap(j,j-1);
		}
		}// bubbleSort
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
