#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */

	 // creates all nodes & adds it to the graph
	 for (int i=0; i<nodeCount; i++){
		 GraphNode* newNode = new GraphNode(i);
		 this->appendNewNode(newNode);
	 }

	//connects nodes
	 for (int j=0; j<nodeCount; j++){
		 for (int k=0; k<nodeCount; k++){
			 if (adjacencies[j][k] !=0){
				 this->nodes[j]->appendNewEdge(nodes[k], adjacencies[j][k]);
			 }

		 }
	 }

}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */
	 nodes[nodesSize]=first;
	 nodesSize++;
	 visited[first->value]=true;
	 for (Edge* e=first->edges; e!=NULL; e=e->next){
		 if (!visited[e->destination->value]){
		 	deepTravel(e->destination,nodes, nodesSize, visited);
		 }
	 }


}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */
	std::queue<GraphNode*> nodeQueue;
	visited[first->value]=false;
	nodeQueue.push(first);
	while(!nodeQueue.empty()){
		GraphNode* g=nodeQueue.back();
		nodeQueue.pop();
        nodes[nodesSize]=g;
        nodesSize++;
        visited[g->value]=true;
		for (Edge* e=first->edges; e!=NULL; e=e->next){	
			if (!visited[e->destination->value]){
		 		nodeQueue.push(e->destination);
		 	}
	 	}

	}
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
 visited[first->value]=true;
    Edge* i=first->edges;
    while (i!=NULL) {
        if(visited[i->destination->value]){
            return true;
        }else{
            detectCycle(i->destination,visited);
        }


    }

    return false;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
