#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot

    // check n
    
    if (n==0){
        return 0;
    }
    else{
        float zx=z.x;
        float zy=z.y;
        float zmodule = sqrt (pow(zx,2)+pow(zy,2));
        // check length of z
        if(zmodule > 2){
            return n;
        }
        else{
            float px=point.x;
            float py=point.y;
            Point zsuivant;
            zsuivant.x = pow(zx,2)-pow(zy,2)+px;
            zsuivant.y=2*zx*zy+py;
            return isMandelbrot(zsuivant,n-1, point);
        }
    }
    
    // if Mandelbrot, return 1 or n (check the difference)
    // otherwise, process the square of z and recall
    // isMandebrot
    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



